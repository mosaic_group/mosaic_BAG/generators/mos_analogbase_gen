v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
C {BAG_prim/nmos4_standard/nmos4_standard.sym} 200 -300 0 0 {name=XM
w=4
l=18n
nf=2
model=nmos4_standard
spiceprefix=X
}
C {devices/iopin.sym} 120 -370 2 0 {name=p5 lab=d}
C {devices/iopin.sym} 120 -350 2 0 {name=p6 lab=g}
C {devices/lab_pin.sym} 220 -330 2 0 {name=l1 sig_type=std_logic lab=d}
C {devices/iopin.sym} 120 -330 2 0 {name=p7 lab=s}
C {devices/iopin.sym} 120 -310 2 0 {name=p8 lab=b}
C {devices/lab_pin.sym} 180 -300 0 0 {name=l2 sig_type=std_logic lab=g}
C {devices/lab_pin.sym} 220 -270 0 0 {name=l3 sig_type=std_logic lab=s}
C {devices/lab_pin.sym} 220 -300 2 0 {name=l4 sig_type=std_logic lab=b}
C {BAG_prim/nmos4_standard/nmos4_standard.sym} 200 -180 0 0 {name=XD
w=4
l=18n
nf=2
model=nmos4_standard
spiceprefix=X
}
C {devices/lab_pin.sym} 220 -210 2 0 {name=l5 sig_type=std_logic lab=b}
C {devices/lab_pin.sym} 180 -180 0 0 {name=l6 sig_type=std_logic lab=b}
C {devices/lab_pin.sym} 220 -150 0 0 {name=l7 sig_type=std_logic lab=b}
C {devices/lab_pin.sym} 220 -180 2 0 {name=l8 sig_type=std_logic lab=b}
