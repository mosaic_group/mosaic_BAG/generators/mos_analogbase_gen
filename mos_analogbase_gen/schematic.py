import os
import pkg_resources
from typing import *

from bag.design import Module

from sal.row import ChannelType
from .params import mos_analogbase_layout_params

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/mos_analogbase_templates',
                         'netlist_info', 'mos_analogbase.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library mos_char_templates cell mos_char.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            layout_params='mos_analogbase_layout_params layout parameter object.',
            dum_info='Dummy information data structure',
        )

    def design(self,
               layout_params: mos_analogbase_layout_params,
               dum_info: List[Tuple[Any]]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        if layout_params.fg == 1:
            raise ValueError('Cannot make 1 finger transistor.')

        # select the correct transistor type

        bag_prim_transistor_prefix: str
        match layout_params.mos_type:
            case ChannelType.N:
                bag_prim_transistor_prefix = "nmos4_"
            case ChannelType.P:
                bag_prim_transistor_prefix = "pmos4_"

        inst_name = 'XM'
        cell_name = f'{bag_prim_transistor_prefix}{layout_params.intent}'
        self.replace_instance_master(inst_name, 'BAG_prim', cell_name)

        if layout_params.stack > 1:
            # array instances
            name_list = []
            term_list = []

            # add stack transistors
            for idx in range(layout_params.stack):
                name_list.append('%s%d<%d:0>' % (inst_name, idx, layout_params.fg - 1))
                cur_term = {}
                if idx != layout_params.stack - 1:
                    cur_term['S'] = 'mid%d<%d:0>' % (idx, layout_params.fg - 1)
                if idx != 0:
                    cur_term['D'] = 'mid%d<%d:0>' % (idx - 1, layout_params.fg - 1)
                term_list.append(cur_term)

            # design transistors
            self.array_instance(inst_name, name_list, term_list=term_list)
            for idx in range(layout_params.stack):
                self.instances[inst_name][idx].design(w=layout_params.w, l=layout_params.lch,
                                                      nf=1,
                                                      intent=layout_params.intent)
        else:
            self.instances[inst_name].design(w=layout_params.w,
                                             l=layout_params.lch,
                                             nf=layout_params.fg,
                                             intent=layout_params.intent)

        # handle dummy transistors
        self.design_dummy_transistors(dum_info, 'XD', 'b', 'b')

