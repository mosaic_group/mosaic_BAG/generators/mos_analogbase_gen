from typing import *

from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackID

from sal.row import ChannelType

from .params import mos_analogbase_layout_params


class layout(AnalogBase):
    """A template of a single transistor with dummies.

    This class is mainly used for transistor characterization or
    design exploration with config views.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='mos_analogbase_layout_params parameter object',
        )

    def draw_layout(self):
        """Draw the layout."""

        params: mos_analogbase_layout_params = self.params['params']

        g_tr_w = params.tr_w_dict['g']
        d_tr_w = params.tr_w_dict['d']
        s_tr_w = params.tr_w_dict['s']
        gs_tr_sp = params.tr_sp_dict['gs']
        gd_tr_sp = params.tr_sp_dict['gd']
        sb_tr_sp = params.tr_sp_dict['sb']
        db_tr_sp = params.tr_sp_dict['db']

        fg_tot = (params.fg * params.stack) + 2 * params.fg_dum
        w_list = [params.w]
        th_list = [params.intent]
        g_tracks = [sb_tr_sp + s_tr_w + gs_tr_sp + g_tr_w]
        ds_tracks = [gd_tr_sp + d_tr_w + db_tr_sp]

        nw_list = pw_list = []
        nth_list = pth_list = []
        ng_tracks = pg_tracks = []
        nds_tracks = pds_tracks = []
        sdir: int
        ddir: int
        match params.mos_type:
            case ChannelType.N:
                nw_list = w_list
                nth_list = th_list
                ng_tracks = g_tracks
                nds_tracks = ds_tracks
                sdir, ddir = 0, 2
            case ChannelType.P:
                pw_list = w_list
                pth_list = th_list
                pg_tracks = g_tracks
                pds_tracks = ds_tracks
                sdir, ddir = 2, 0
            case _:
                raise ValueError(f"Unsupported channel type {params.mos_type}")

        self.draw_base(params.lch, fg_tot, params.ptap_w, params.ntap_w,
                       nw_list, nth_list,
                       pw_list, pth_list,
                       ng_tracks=ng_tracks, nds_tracks=nds_tracks,
                       pg_tracks=pg_tracks, pds_tracks=pds_tracks)

        mos_type = params.mos_type.value
        mos_ports = self.draw_mos_conn(mos_type=mos_type,
                                       row_idx=0,
                                       col_idx=params.fg_dum,
                                       fg=params.fg * params.stack,
                                       sdir=sdir,
                                       ddir=ddir,
                                       stack=params.stack, s_net='s', d_net='d')
        tr_id = self.make_track_id(mos_type, 0, 'g', sb_tr_sp + (s_tr_w - 1) / 2, width=s_tr_w)
        warr = self.connect_to_tracks(mos_ports['s'], tr_id)
        self.add_pin('s', warr, show=True)

        tr_id = self.make_track_id(mos_type, 0, 'g', sb_tr_sp + s_tr_w + gs_tr_sp + (g_tr_w - 1) / 2, width=g_tr_w)
        warr = self.connect_to_tracks(mos_ports['g'], tr_id,
                                      min_len_mode=0)  # Respect min area rules (otherwise DRC issue with sky130A)
        self.add_pin('g', warr, show=True)

        tr_id = self.make_track_id(mos_type, 0, 'ds', gd_tr_sp + (d_tr_w - 1) / 2, width=d_tr_w)
        warr = self.connect_to_tracks(mos_ports['d'], tr_id)
        self.add_pin('d', warr, show=True)

        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()

        # export body
        self.add_pin('b', ptap_wire_arrs, show=True)
        self.add_pin('b', ntap_wire_arrs, show=True)

        # compute schematic parameters
        self._sch_params = dict(
            layout_params=params,
            dum_info=self.get_sch_dummy_info(),
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class mos_analogbase(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
