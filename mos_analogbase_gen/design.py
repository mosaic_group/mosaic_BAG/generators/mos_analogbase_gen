"""
mos_analogbase
========

"""

from typing import *

from bag.layout.template import TemplateBase
from sal.simulation.measurement_base import MeasurementBase
from sal.params_base import GeneratorParamsBase
from sal.design_base import DesignBase

from .params import mos_analogbase_params
from .layout import layout as mos_analogbase_layout
from .measure import mos_analogbase_measurement_mos_id, mos_analogbase_measurement_mos_sp


class design(DesignBase):
    def __init__(self):
        super().__init__()
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self) -> str:
        return "mos_analogbase_gen"

    @classmethod
    def layout_generator_class(cls) -> Optional[Type[TemplateBase]]:
        """Return the layout generator class"""
        return mos_analogbase_layout

    @classmethod
    def parameter_class(cls) -> Type[GeneratorParamsBase]:
        """Return the parameter class"""
        return mos_analogbase_params

    @classmethod
    def measurement_classes(cls) -> List[Type[MeasurementBase]]:
        """Return a list of measurement classes"""
        return [
            mos_analogbase_measurement_mos_id,
            mos_analogbase_measurement_mos_sp
        ]

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> mos_analogbase_params:
        return self._params

    @params.setter
    def params(self, val: mos_analogbase_params):
        self._params = val
