import matplotlib.pyplot as plt
import numpy as np
import os
from pprint import pprint
import scipy.interpolate
import scipy.optimize
from typing import *

import bag.data

from sal.log import info, error
from sal.testbench_base import TestbenchBase
from sal.simulation.measurement_base import *
from sal.simulation.simulation_mode import SimulationMode

from .params import mos_analogbase_measurement_mos_id_params, mos_analogbase_measurement_mos_sp_params

from mos_id_tb.testbench import testbench as mos_id_testbench
from mos_sp_tb.testbench import testbench as mos_sp_testbench


class mos_analogbase_measurement_state(str, MeasurementState):
    PLOT = 'plot'
    END = 'end'

    @classmethod
    def initial_state(cls) -> MeasurementState:
        return cls.PLOT

    @classmethod
    def final_states(cls) -> Set[MeasurementState]:
        return {cls.END}


class mos_analogbase_measurement_mos_id(MeasurementBase):
    @classmethod
    def name(cls) -> str:
        return 'mos_id'

    @classmethod
    def description(cls) -> str:
        return 'Measurement class for transistor characterization.'

    @classmethod
    def parameter_class(cls) -> Type[MeasurementParamsBase]:
        return mos_analogbase_measurement_mos_id_params

    @classmethod
    def fsm_state_class(cls) -> Type[MeasurementState]:
        return mos_analogbase_measurement_state

    @classmethod
    def mos_id_testbench_setup(cls) -> TestbenchSetup:
        return TestbenchSetup(
            testbench_name='mos_id_tb',
            testbench_class=mos_id_testbench,
            dut_wrapper=None
        )

    @classmethod
    def testbench_setup_list(cls) -> List[TestbenchSetup]:
        return [
            cls.mos_id_testbench_setup()
        ]

    def __init__(self, params: mos_analogbase_measurement_mos_id_params):
        self.params = params

    def prepare_testbench(self,
                          current_state: mos_analogbase_measurement_state,
                          previous_output: Optional[Dict[str, Any]]) -> TestbenchRun:
        setup = self.mos_id_testbench_setup()
        tb_params = self.params.testbench_params_by_name['mos_id_tb'].copy()
        # no need to patch params
        return TestbenchRun(setup=setup, params=tb_params)

    def process_output(self,
                       current_state: mos_analogbase_measurement_state,
                       current_output: Dict[str, Any],
                       testbench_run: TestbenchRun) -> (mos_analogbase_measurement_state,
                                                        Dict[str, Any]):
        info("MOS Id testbench post-processing...")

        if self.params.plot:
            result_list = split_data_by_sweep(current_output, ['ibias'])

            tvec = current_output['vgs']
            plot_data_list = []
            for label, res_dict in result_list:
                cur_vout = res_dict['ibias']
                plot_data_list.append((label, cur_vout))

            if self.params.plot:
                plt.figure()
                plt.title('Id vs Vgs')
                plt.ylabel('Id (I)')
                plt.xlabel('Vgs (V)')

                for label, cur_vout in plot_data_list:
                    if label:
                        plt.plot(tvec, cur_vout, label=label)
                    else:
                        plt.plot(tvec, cur_vout)

                if len(result_list) > 1:
                    plt.legend()

                plt.show()

            return mos_analogbase_measurement_state.END, current_output


class mos_analogbase_measurement_mos_sp(MeasurementBase):
    @classmethod
    def name(cls) -> str:
        return 'mos_sp'

    @classmethod
    def description(cls) -> str:
        return 'Measurement class for extracting transistor small-signal parameters.'

    @classmethod
    def parameter_class(cls) -> Type[MeasurementParamsBase]:
        return mos_analogbase_measurement_mos_sp_params

    @classmethod
    def fsm_state_class(cls) -> Type[MeasurementState]:
        return mos_analogbase_measurement_state

    @classmethod
    def mos_sp_testbench_setup(cls) -> TestbenchSetup:
        return TestbenchSetup(
            testbench_name='mos_sp_tb',
            testbench_class=mos_sp_testbench,
            dut_wrapper=None
        )

    @classmethod
    def testbench_setup_list(cls) -> List[TestbenchSetup]:
        return [
            cls.mos_sp_testbench_setup()
        ]

    def __init__(self, params: mos_analogbase_measurement_mos_sp_params):
        self.params = params

    def prepare_testbench(self,
                          current_state: mos_analogbase_measurement_state,
                          previous_output: Optional[Dict[str, Any]]) -> TestbenchRun:
        setup = self.mos_sp_testbench_setup()
        tb_params = self.params.testbench_params_by_name['mos_sp_tb'].copy()
        # no need to patch params
        return TestbenchRun(setup=setup, params=tb_params)

    def process_output(self,
                       current_state: mos_analogbase_measurement_state,
                       current_output: Dict[str, Any],
                       testbench_run: TestbenchRun) -> (mos_analogbase_measurement_state,
                                                        Dict[str, Any]):
        info("MOS SP testbench post-processing...")

        # if self.params.plot:
        #     result_list = split_data_by_sweep(current_output, ['ibias'])
        #
        #     tvec = current_output['vgs']
        #     plot_data_list = []
        #     for label, res_dict in result_list:
        #         cur_vout = res_dict['ibias']
        #         plot_data_list.append((label, cur_vout))
        #
        #     if self.params.plot:
        #         plt.figure()
        #         plt.title('Id vs Vgs')
        #         plt.ylabel('Id (I)')
        #         plt.xlabel('Vgs (V)')
        #
        #         for label, cur_vout in plot_data_list:
        #             if label:
        #                 plt.plot(tvec, cur_vout, label=label)
        #             else:
        #                 plt.plot(tvec, cur_vout)
        #
        #         if len(result_list) > 1:
        #             plt.legend()
        #
        #         plt.show()

        return mos_analogbase_measurement_state.END, current_output
