#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.row import ChannelType
from sal.testbench_base import TestbenchBase
from sal.testbench_params import *

from mos_id_tb.testbench import testbench as mos_id_tb_testbench
from mos_id_tb.params import mos_id_tb_params
from mos_sp_tb.testbench import testbench as mos_sp_tb_testbench
from mos_sp_tb.params import mos_sp_tb_params


@dataclass
class mos_analogbase_layout_params(LayoutParamsBase):
    """
    Parameter class for mos_analogbase_gen

    Args:
    ----

    mos_type
        transistor type, either 'pch' or 'nch'.

    lch
        Channel length of the transistors, in meters.

    w
        transistor width, in meters/number of fins.

    intent
        transistor threshold flavor.

    stack
        number of transistors to stack

    fg
        number of fingers.

    fg_dum
        number of dummies on each side.

    ptap_w
        NMOS substrate width, in meters/number of fins.

    ntap_w
        PMOS substrate width, in meters/number of fins.

    tr_w_dict
        track width dictionary.

    tr_sp_dict
        track space dictionary.

    show_pins : bool
        True to create pin labels
    """

    mos_type: ChannelType
    lch: float
    w: Union[float, int]
    intent: str
    stack: int
    fg: int
    fg_dum: int
    ptap_w: Union[float, int]
    ntap_w: Union[float, int]
    tr_w_dict: Dict[str, int]
    tr_sp_dict: Dict[str, int]
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> mos_analogbase_layout_params:
        return mos_analogbase_layout_params(
            mos_type=ChannelType.N,
            lch=min_lch,
            w=10,
            intent='standard',
            stack=1,
            fg=2,
            fg_dum=4,
            ptap_w=10,
            ntap_w=10,
            tr_w_dict={
                'g': 1,
                'd': 2,
                's': 2,
            },
            tr_sp_dict={
                'gs': 1,
                'gd': 1,
                'sb': 1,
                'db': 1,
            },
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> mos_analogbase_layout_params:
        return mos_analogbase_layout_params(
            mos_type=ChannelType.N,
            lch=min_lch,
            w=10 * min_lch,
            intent='standard',
            stack=1,
            fg=2,
            fg_dum=4,
            ptap_w=10 * min_lch,
            ntap_w=10 * min_lch,
            tr_w_dict={
                'g': 1,
                'd': 2,
                's': 2,
            },
            tr_sp_dict={
                'gs': 1,
                'gd': 1,
                'sb': 1,
                'db': 1,
            },
            show_pins=True,
        )


@dataclass
class mos_analogbase_measurement_mos_id_params(MeasurementParamsBase):
    plot: bool
    testbench_params_by_name: Dict[str, TestbenchParams]  # Key: testbench name

    @classmethod
    def defaults(cls, min_lch: float) -> mos_analogbase_measurement_mos_id_params:
        dut = DUT(lib="mos_analogbase_generated", cell="mos_analogbase")

        mos_id_defaults = mos_id_tb_params(
            dut=dut,  # NOTE: Framework will inject DUT
            dut_wrapper_params=None,
            sch_params=TestbenchSchematicParams(
                dut_conns=[
                    DUTTerminal(term_name='vbias', net_name='vbias'),
                ],
                v_sources=[
                    DCSignalSource(source_name='vbias',
                                   plus_net_name='vbias',
                                   minus_net_name='VSS',
                                   bias_value='vbias',
                                   cdf_parameters={}),
                ],
                i_sources=[],
                instance_cdf_parameters={}
            ),
            simulation_params=TestbenchSimulationParams(
                variables={},
                sweeps={},
                outputs={}
            ),
            gain_fb=400.0,
            vimax=1.0,
            vimin=1.0,
            vbias=0.185,
            vdd=1.0,
            voutref=0.5,
            vout_start=0.1,
            vout_stop=0.9,
            vout_num=100,
        )

        return mos_analogbase_measurement_mos_id_params(
            plot=True,
            testbench_params_by_name={
                'mos_id_tb': mos_id_defaults,
            }
        )


@dataclass
class mos_analogbase_measurement_mos_id_params(MeasurementParamsBase):
    plot: bool
    testbench_params_by_name: Dict[str, TestbenchParams]  # Key: testbench name

    @classmethod
    def defaults(cls, min_lch: float) -> mos_analogbase_measurement_mos_id_params:
        dut = DUT(lib="mos_analogbase_generated", cell="mos_analogbase")

        mos_id_defaults = mos_id_tb_params.defaults()
       
        return mos_analogbase_measurement_mos_id_params(
            plot=True,
            testbench_params_by_name={
                'mos_id_tb': mos_id_defaults,
            }
        )


@dataclass
class mos_analogbase_measurement_mos_sp_params(MeasurementParamsBase):
    plot: bool
    testbench_params_by_name: Dict[str, TestbenchParams]  # Key: testbench name

    @classmethod
    def defaults(cls, min_lch: float) -> mos_analogbase_measurement_mos_sp_params:
        mos_sp_defaults = mos_sp_tb_params.defaults()

        return mos_analogbase_measurement_mos_sp_params(
            plot=True,
            testbench_params_by_name={
                'mos_sp_tb': mos_sp_defaults,
            }
        )


@dataclass
class mos_analogbase_params(GeneratorParamsBase):
    layout_parameters: mos_analogbase_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> mos_analogbase_params:
        return mos_analogbase_params(
            layout_parameters=mos_analogbase_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[
                mos_analogbase_measurement_mos_id_params.defaults(min_lch=min_lch),
                mos_analogbase_measurement_mos_sp_params.defaults(min_lch=min_lch)
            ]
        )
